# Kozii-News

Kozii-News is a simple project to study and play with some android components, architecture and tools for Android development.

## Tech Stack

This project uses feature modularization architecture, with some modules using MVVM and others just MVI.

## Development setup

You require the latest Android Studio 4.2 (stable channel) to be able to build the app.
