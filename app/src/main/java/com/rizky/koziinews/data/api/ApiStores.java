package com.rizky.koziinews.data.api;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiStores {
    @GET("articlesearch.json")
    Single<KoziiResponse<DocResponse>> getArticleSearch(
            @Query("q") String query,
            @Query("sort") String sort,
            @Query("page") Integer page);
}
