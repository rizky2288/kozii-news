package com.rizky.koziinews.data.database;

import androidx.paging.PagingSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.rizky.koziinews.object.Doc;

import java.util.List;

@Dao
public abstract class DocDAO {

    @Query("DELETE FROM " + DB.TABLE_NEWS)
    public abstract void deleteDoc();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertDoc(List<Doc> docList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertDoc(Doc doc);

    @Transaction
    public void updateDoc(List<Doc> docList) {
        deleteDoc();
        for (Doc doc : docList) {
            doc.setHeadline_main(doc.getHeadline().getMain());
            insertDoc(doc);
        }
    }

    @Query("SELECT * FROM " + DB.TABLE_NEWS +
            " WHERE " + DB.COLUMN_ID +
            " IN (SELECT " + DB.COLUMN_ID +
            " FROM " + DB.TABLE_NEWS +
            " ORDER BY " + DB.COLUMN_DATE_CREATED +
            " LIMIT :size) ")
    public abstract List<Doc> getDocList(int size);

    @Query("SELECT * FROM " + DB.TABLE_NEWS)
    public abstract List<Doc> getDocList();

    @Query("SELECT * FROM " + DB.TABLE_NEWS +
            " ORDER BY " + DB.COLUMN_DATE_CREATED +
            " DESC ")
    public abstract PagingSource<Integer, Doc> getDocListPaging();
}
