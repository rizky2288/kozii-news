package com.rizky.koziinews.data.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.rizky.koziinews.object.Doc;
import com.rizky.koziinews.object.Search;

@Database(entities = {Doc.class, Search.class},
        version = KoziiLocalDB.DATABASE_VERSION)
public abstract class KoziiLocalDB extends RoomDatabase {
    static final int DATABASE_VERSION = 8;
    private static KoziiLocalDB INSTANCE;

    private static final String DATABASE_NAME = "KoziiNews.db";

    public static synchronized KoziiLocalDB getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    KoziiLocalDB.class,
                    DATABASE_NAME)
                    .build();
            return INSTANCE;
        }
        return INSTANCE;
    }

    public abstract DocDAO docDAO();
    public abstract SearchDAO searchDAO();
}
