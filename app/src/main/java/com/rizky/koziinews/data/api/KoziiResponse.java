package com.rizky.koziinews.data.api;

public class KoziiResponse<T> {
    private String status;

    private String copyright;

    private T response;

    public KoziiResponse(String status, String copyright, T response) {
        this.status = status;
        this.copyright = copyright;
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
