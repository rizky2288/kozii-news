package com.rizky.koziinews.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rizky.koziinews.object.Search;

import java.util.List;

@Dao
public abstract class SearchDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertSearch(Search search);

    @Query("SELECT * FROM " + DB.TABLE_SEARCH +
            " ORDER BY " + DB.COLUMN_DATE_CREATED + " DESC " +
            " LIMIT 10")
    public abstract List<Search> getSearchHistory();
}
