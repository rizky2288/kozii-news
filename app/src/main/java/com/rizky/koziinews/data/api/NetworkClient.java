package com.rizky.koziinews.data.api;

import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {
    private static Retrofit retrofit;
    private static OkHttpClient okHttpClient;

    public static String API_KEY = "tnYmXRvAMGkwIAXYGp2ZkmyJcrmu1PF0";
    public static String KOZII_BASE_URL = "https://api.nytimes.com/svc/search/v2/";
    public static String IMAGE_BASE_URL = "https://www.nytimes.com/";

    public static void CancelAllRequest() {
        okHttpClient.dispatcher().cancelAll();
    }

    public static Retrofit getRetrofit() {
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    HttpUrl originalHttpUrl = original.url();
                    HttpUrl url = originalHttpUrl.newBuilder()
                            // Add API Key as query string parameter
                            .addQueryParameter("api-key", API_KEY)
                            .build();
                    Request.Builder requestBuilder = original.newBuilder()
                            .url(url);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                })
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(KOZII_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }
}
