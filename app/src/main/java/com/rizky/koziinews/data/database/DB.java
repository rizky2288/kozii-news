package com.rizky.koziinews.data.database;

public class DB {
    public final static String TABLE_NEWS = "news";
    public final static String TABLE_SEARCH = "search";
    public final static String COLUMN_ID = "id";
    public final static String COLUMN_SEARCH_HISTORY = "search_history";
    public final static String COLUMN_SECTION = "section";
    public final static String COLUMN_SNIPPET = "snippet";
    public final static String COLUMN_HEADLINE = "headline";
    public final static String COLUMN_IMG_BITMAP = "img_bitmap";
    public final static String COLUMN_WEB_URI = "web_uri";
    public final static String COLUMN_DATE_CREATED = "created_at";
}
