package com.rizky.koziinews.data.api.body;

public class SearchArticelBody {
    private Integer page;
    private String query;

    public SearchArticelBody(Integer page, String query) {
        this.page = page;
        this.query = query;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
