package com.rizky.koziinews.data;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class KoziiSharedPref {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private static final String PREF_NAME = "KoziiSharedPref";

    public static final String KEY_DOCLIST = "doclist";
    public static final String KEY_DOCLIST_LOCAL = "doclist_local";

    public KoziiSharedPref(Context context) {
        this._context = context;
        int PRIVATE_MODE = 0;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public HashMap<String, String> getSessionData() {
        HashMap<String, String> sessionData = new HashMap<>();
        sessionData.put(KEY_DOCLIST, pref.getString(KEY_DOCLIST, null));
        return sessionData;
    }

    public void modifySession(String column, String value) {
        editor.putString(column, value);
        editor.commit();
    }
}
