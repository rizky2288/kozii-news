package com.rizky.koziinews.data.api;

import com.rizky.koziinews.object.Doc;

import java.util.List;

public class DocResponse {
    private List<Doc> docs;

    public DocResponse(List<Doc> docs) {
        this.docs = docs;
    }

    public List<Doc> getDocs() {
        return docs;
    }

    public void setDocs(List<Doc> docs) {
        this.docs = docs;
    }
}
