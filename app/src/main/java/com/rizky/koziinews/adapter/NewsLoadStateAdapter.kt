package com.rizky.koziinews.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rizky.koziinews.R
import com.rizky.koziinews.adapter.NewsLoadStateAdapter.LoadStateViewHolder
import com.rizky.koziinews.databinding.LoadStateItemBinding

class NewsLoadStateAdapter(private val mRetryCallback: View.OnClickListener) :
    LoadStateAdapter<LoadStateViewHolder>() {
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        loadState: LoadState): LoadStateViewHolder {
        return LoadStateViewHolder(viewGroup, mRetryCallback)
    }

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    inner class LoadStateViewHolder internal constructor (
        parent: ViewGroup,
        retryCallback: View.OnClickListener) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.load_state_item, parent, false)) {
        private val mProgressBar: ProgressBar

        private val mErrorMsg: TextView

        private val mRetry: Button
        fun bind(loadState: LoadState?) {
            if (loadState is Error) {
                val loadStateError = loadState as Error
                mErrorMsg.text = loadStateError.localizedMessage
            }
            mProgressBar.visibility = if (loadState is LoadState.Loading) View.VISIBLE else View.GONE
            mRetry.visibility = if (loadState is Error) View.VISIBLE else View.GONE
            mErrorMsg.visibility =
                if (loadState is Error) View.VISIBLE else View.GONE
        }

        init {
            val binding = LoadStateItemBinding.bind(itemView)
            mProgressBar = binding.progressBar
            mErrorMsg = binding.errorMsg
            mRetry = binding.retryButton
            mRetry.setOnClickListener(retryCallback)
        }
    }
}