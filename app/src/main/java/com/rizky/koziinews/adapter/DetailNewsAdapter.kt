package com.rizky.koziinews.adapter

import android.app.Activity
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.recyclerview.widget.RecyclerView
import com.rizky.koziinews.`object`.Doc
import com.rizky.koziinews.databinding.ItemNewsPagerBinding

class DetailNewsAdapter(private val activity: Activity, private val mData: List<Doc>)
    : RecyclerView.Adapter<DetailNewsAdapter.TeamNewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamNewsViewHolder {
        return TeamNewsViewHolder(ItemNewsPagerBinding.inflate((parent.context as Activity).layoutInflater, parent, false))
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: TeamNewsViewHolder, position: Int) {
        val doc = mData[position]

        holder.binding.progressBar.isIndeterminate = true
        holder.binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                holder.binding.progressBar.visibility = GONE
                holder.binding.webView.visibility = VISIBLE
                super.onPageFinished(view, url)
            }
        }
        doc.let { holder.binding.webView.loadUrl(it.web_url) }
    }

    class TeamNewsViewHolder(val binding: ItemNewsPagerBinding) : RecyclerView.ViewHolder(binding.root)
}
