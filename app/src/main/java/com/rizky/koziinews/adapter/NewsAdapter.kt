package com.rizky.koziinews.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rizky.koziinews.KoziiMethods
import com.rizky.koziinews.R
import com.rizky.koziinews.`object`.Doc
import com.rizky.koziinews.`object`.Headline
import com.rizky.koziinews.`object`.Multimedia
import com.rizky.koziinews.data.api.NetworkClient
import com.rizky.koziinews.databinding.CardItemNewsBinding
import java.util.*

class NewsAdapter(private val context: Context) : PagingDataAdapter<Doc, NewsAdapter.ViewHolder>(DOC_ITEM_CALLBACK) {
    private var listener: NewsAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(CardItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val doc = getItem(position)
        if (doc!!.headline == null) {
            val headline = Headline()
            headline.main = doc.headline_main
            doc.headline = headline
        }
        holder.binding.txtSection.text = doc.section_name
        holder.binding.txtHeadline.text = doc.headline.main
        holder.binding.txtSnippet.text = doc.snippet
        if (doc.multimedia == null) {
            val tempMultimediaList: List<Multimedia> = ArrayList<Multimedia>()
            doc.multimedia = tempMultimediaList
        }
        var thumbUrl = ""
        doc.multimedia.forEach { multimedia ->
            if (multimedia.type == "image" && multimedia.subType == "master315") {
                thumbUrl = NetworkClient.IMAGE_BASE_URL + multimedia.url
            }
        }
        holder.binding.imgThumbnail.setImageDrawable(context.getDrawable(R.drawable.logo_with_background))
        if (!TextUtils.isEmpty(thumbUrl)) {
            Glide.with(context)
                .load(thumbUrl)
                .fitCenter()
                .error(R.drawable.logo_with_background)
                .into(holder.binding.imgThumbnail)
        }
        holder.binding.txtTanggal.text = KoziiMethods.ConvertStringDateIntoDisplayDate(doc.pub_date)
        holder.binding.root.setOnClickListener {
            if (listener != null) {
                listener!!.onItemClicked(doc, position)
            }
        }
    }

    inner class ViewHolder(val binding: CardItemNewsBinding) : RecyclerView.ViewHolder(
        binding.root
    )

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount) NEWS_ITEM else LOADING_ITEM
    }

    fun setListener(listener: NewsAdapterListener?) {
        this.listener = listener
    }

    interface NewsAdapterListener {
        fun onItemClicked(doc: Doc?, position: Int)
    }

    companion object {
        const val LOADING_ITEM = 0
        const val NEWS_ITEM = 1
        val DOC_ITEM_CALLBACK: DiffUtil.ItemCallback<Doc> = object : DiffUtil.ItemCallback<Doc>() {
            override fun areItemsTheSame(oldItem: Doc, newItem: Doc): Boolean {
                return oldItem._id == newItem._id
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Doc, newItem: Doc): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }
        }
    }
}