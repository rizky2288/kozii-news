package com.rizky.koziinews.viewmodel;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.rxjava3.PagingRx;

import com.rizky.koziinews.KoziiMethods;
import com.rizky.koziinews.data.api.ApiStores;
import com.rizky.koziinews.data.api.NetworkClient;
import com.rizky.koziinews.data.api.body.SearchArticelBody;
import com.rizky.koziinews.data.database.KoziiLocalDB;
import com.rizky.koziinews.object.Doc;
import com.rizky.koziinews.paging.ArticleRemotePagingDataSource;

import io.reactivex.rxjava3.core.Flowable;
import kotlinx.coroutines.CoroutineScope;

public class MainActivityViewModel extends ViewModel {
    private final Application application;
    private final SearchArticelBody body;

    public Flowable<PagingData<Doc>> pagingDataFlow;

    public MutableLiveData<Integer> mNewsSize = new MutableLiveData<>();

    public MainActivityViewModel(Application application, SearchArticelBody body) {
        this.application = application;
        this.body = body;
        init();
    }

    private void init() {
        ApiStores apiStores = NetworkClient.getRetrofit().create(ApiStores.class);
        KoziiLocalDB localDB = KoziiLocalDB.getInstance(application.getBaseContext());
        ArticleRemotePagingDataSource dataSource = new ArticleRemotePagingDataSource(apiStores, localDB, body);

        Pager<Integer, Doc> pager = new Pager<>(
                new PagingConfig(10,
                        10,
                        false,
                        10),
                () ->dataSource
        );

        Pager<Integer, Doc> pagerLocal = new Pager<>(
                new PagingConfig(10,
                        10,
                        false,
                        10),
                () -> localDB.docDAO().getDocListPaging()
        );

        if (KoziiMethods.checkInternet(application.getBaseContext())) {
            pagingDataFlow = PagingRx.getFlowable(pager);
        } else {
            pagingDataFlow = PagingRx.getFlowable(pagerLocal);
        }
        CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
        PagingRx.cachedIn(pagingDataFlow, viewModelScope);

        mNewsSize = dataSource.getmNewsSize();
    }
}
