package com.rizky.koziinews.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rizky.koziinews.data.api.body.SearchArticelBody

class MainActivityViewModelFactory(
    private val application: Application,
    private val body: SearchArticelBody) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainActivityViewModel(application, body) as T
    }

}