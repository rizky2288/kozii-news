package com.rizky.koziinews;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.rizky.koziinews.databinding.CustomErrorAlertBinding;

public class ErrorAlertDialog extends Dialog implements View.OnClickListener {
    CustomErrorAlertBinding binding;

    private Activity activity;
    private String posisiError;
    private String error;

    public ErrorAlertDialog(@NonNull Activity activity, String posisiError, String error) {
        super(activity);
        this.activity = activity;
        this.posisiError = posisiError;
        this.error = error;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = CustomErrorAlertBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnNo.setOnClickListener(this);
        binding.btnDetail.setOnClickListener(this);
        binding.closeDialog.setOnClickListener(this);
        binding.txtDialog.setText("Terjadi Error pada Aplikasi");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.closeDialog:
            case R.id.btn_no:
                dismiss();
                break;
            case R.id.btn_detail:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    binding.txtDialog.setText(Html.fromHtml(error, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
                } else {
                    binding.txtDialog.setText(Html.fromHtml(error), TextView.BufferType.SPANNABLE);
                }
                binding.btnDetail.setVisibility(View.GONE);
                break;
        }
    }

    public void hideDetailButton() {
        binding.btnDetail.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.txtDialog.setText(Html.fromHtml(error, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            binding.txtDialog.setText(Html.fromHtml(error), TextView.BufferType.SPANNABLE);
        }
    }

    public void hideAllButton(){
        binding.btnNo.setVisibility(View.GONE);
        binding.btnDetail.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.txtDialog.setText(Html.fromHtml(error, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            binding.txtDialog.setText(Html.fromHtml(error), TextView.BufferType.SPANNABLE);
        }
    }
}
