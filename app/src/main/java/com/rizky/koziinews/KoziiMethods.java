package com.rizky.koziinews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class KoziiMethods {

    public static String ConvertStringDateIntoDisplayDate(String raw) {
        try {
            SimpleDateFormat displayDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ", Locale.getDefault());
            Date input = displayDate.parse(raw);
            SimpleDateFormat formatsource = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            return formatsource.format(input);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void showAlert(Activity activity, String message) {
        if (!activity.isFinishing()) {
            ErrorAlertDialog errorAlertDialog = new ErrorAlertDialog(activity, null, message);
            errorAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            errorAlertDialog.setCancelable(true);
            errorAlertDialog.show();
            errorAlertDialog.hideAllButton();
        }
    }

    public static boolean checkInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static String GetDateNowString() {
        SimpleDateFormat formatsource = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        return formatsource.format(new Date());
    }
}
