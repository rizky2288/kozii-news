package com.rizky.koziinews.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource.LoadResult.Page.Companion.COUNT_UNDEFINED
import androidx.paging.PagingState
import androidx.paging.rxjava3.RxPagingSource
import com.rizky.koziinews.`object`.Doc
import com.rizky.koziinews.data.api.ApiStores
import com.rizky.koziinews.data.api.DocResponse
import com.rizky.koziinews.data.api.KoziiResponse
import com.rizky.koziinews.data.api.body.SearchArticelBody
import com.rizky.koziinews.data.database.KoziiLocalDB
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class ArticleRemotePagingDataSource(
    private val mApiStores: ApiStores,
    private val localDB: KoziiLocalDB?,
    private val body: SearchArticelBody) : RxPagingSource<Int, Doc>() {
    private val mNewsSize = MutableLiveData<Int>()
    override fun loadSingle(loadParams: LoadParams<Int>): Single<LoadResult<Int, Doc>> {
        return try {
            val page = if (loadParams.key == null) 1 else loadParams.key!!
            val sort = "newest"
            body.page = page
            mApiStores.getArticleSearch(body.query, sort, page)
                .subscribeOn(Schedulers.io())
                .map { response: KoziiResponse<DocResponse> -> toLoadResult(response) }
                .onErrorReturn { LoadResult.Error(it) }
        } catch (e: Exception) {
            e.printStackTrace()
            Single.just(Error(e)) as Single<LoadResult<Int, Doc>>
        }
    }

    private fun toLoadResult(response: KoziiResponse<DocResponse>): LoadResult<Int, Doc> {
        if (body.page == 1) {
            Thread {
                if (localDB != null && response.response.docs != null) {
                    localDB.docDAO().updateDoc(response.response.docs)
                }
            }.start()
        }
        mNewsSize.postValue(response.response.docs.size)
        return LoadResult.Page(
            response.response.docs,
            if (body.page == 1) null else body.page - 1,
            if (response.response.docs.size < 10) null else body.page + 1,
            COUNT_UNDEFINED,
            COUNT_UNDEFINED
        )
    }

    override fun getRefreshKey(pagingState: PagingState<Int, Doc>): Int? {
        val anchorPosition = pagingState.anchorPosition ?: return null
        val (_, prevKey, nextKey) = pagingState.closestPageToPosition(anchorPosition)
            ?: return null
        if (prevKey != null) {
            return prevKey + 1
        }
        return if (nextKey != null) {
            nextKey - 1
        } else null
    }

    fun getmNewsSize(): MutableLiveData<Int> {
        return mNewsSize
    }
}