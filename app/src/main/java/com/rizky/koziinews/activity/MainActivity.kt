package com.rizky.koziinews.activity

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.MenuItemCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.rizky.koziinews.KoziiMethods
import com.rizky.koziinews.R
import com.rizky.koziinews.`object`.Search
import com.rizky.koziinews.adapter.NewsAdapter
import com.rizky.koziinews.adapter.NewsLoadStateAdapter
import com.rizky.koziinews.data.KoziiSharedPref
import com.rizky.koziinews.data.api.body.SearchArticelBody
import com.rizky.koziinews.data.database.KoziiLocalDB
import com.rizky.koziinews.databinding.ActivityMainBinding
import com.rizky.koziinews.viewmodel.MainActivityViewModel
import com.rizky.koziinews.viewmodel.MainActivityViewModelFactory
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private var localDb : KoziiLocalDB? = null
    private var session : KoziiSharedPref? = null
    private var progressDialog : ProgressDialog? = null
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        title = null
        setSupportActionBar(binding.toolbarNews)

        localDb = KoziiLocalDB.getInstance(this)
        session = KoziiSharedPref(this)

        setListener()
        getArticle("")
    }

    private fun setListener() {
        binding.pullRefreshLayout.setOnRefreshListener {
            getArticle("")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        setupSearchView(menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun setupSearchView(menu: Menu?) {
        val menuItem = menu!!.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(menuItem) as SearchView
        searchView.inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS
        searchView.queryHint = "Search"
        searchView.isIconifiedByDefault = false
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                val thread = Thread {
                    try {
                        val search = Search(query.toString())
                        localDb?.searchDAO()?.insertSearch(search)
                    } catch (e:Exception) {
                        Log.d("SearchHistory", e.message.toString())
                    }
                }
                thread.start()
                getArticle(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (TextUtils.isEmpty(newText)) {
                    getArticle("")
                }
                return false
            }
        })
    }

    private fun getArticle(query: String?) {
        showLoading("Mohon tunggu")
        viewModelStore.clear()
        val body = SearchArticelBody(1, query)

        binding.recyclerNews.layoutManager = LinearLayoutManager(this)
        val adapter = NewsAdapter(this)
        adapter.setListener { doc, position ->
            val intent = Intent(this@MainActivity, DetailActivity::class.java)
            val docList = adapter.snapshot()
            session?.modifySession(KoziiSharedPref.KEY_DOCLIST, gson.toJson(docList))
            intent.putExtra("position", position.toString())
            startActivity(intent)
        }

        binding.recyclerNews.adapter = adapter
            .withLoadStateFooter(NewsLoadStateAdapter { v: View? -> adapter.retry() })

        val viewModel = ViewModelProvider(this, MainActivityViewModelFactory(this.application, body)).get(MainActivityViewModel::class.java)

        viewModel.pagingDataFlow
            .subscribe({ customerPagingData ->
            adapter.submitData(this@MainActivity.lifecycle, customerPagingData)
                hideLoading()
            }) { throwable ->
            hideLoading()
                Toast.makeText(baseContext, throwable.localizedMessage, Toast.LENGTH_LONG).show()
            }

        viewModel.mNewsSize.observe(this, { newSize ->
            if (newSize == 0 && adapter.snapshot().size == 0) {
                KoziiMethods.showAlert(this, "Tidak ada berita")
            }
        })
    }

    private fun showLoading(message : String) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(this)
            progressDialog!!.setMessage(message)
        } else {
            progressDialog!!.setMessage(message)
        }
        progressDialog!!.show()
        binding.pullRefreshLayout.isRefreshing = true
    }

    private fun hideLoading() {
        progressDialog?.dismiss()
        binding.pullRefreshLayout.isRefreshing = false
    }
}