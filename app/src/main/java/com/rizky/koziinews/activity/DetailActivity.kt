package com.rizky.koziinews.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rizky.koziinews.`object`.Doc
import com.rizky.koziinews.adapter.DetailNewsAdapter
import com.rizky.koziinews.data.KoziiSharedPref
import com.rizky.koziinews.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {
    private lateinit var binding : ActivityDetailBinding
    private lateinit var adapter : DetailNewsAdapter
    private var session : KoziiSharedPref? = null
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = null
        setSupportActionBar(binding.toolbarNews)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.toolbarNews.setNavigationOnClickListener {
            onBackPressed()
        }

        session = KoziiSharedPref(this)

        val clickedPosition = intent.getStringExtra("position")
        val type = object : TypeToken<List<Doc>?>() {}.type
        var docList : List<Doc> = gson.fromJson(session!!.sessionData[KoziiSharedPref.KEY_DOCLIST], type)

        adapter = DetailNewsAdapter(this, docList)
        binding.viewPagerWeb.adapter = adapter
        binding.viewPagerWeb.offscreenPageLimit = 3

        if (clickedPosition != null) {
            binding.viewPagerWeb.currentItem = clickedPosition.toInt()
        }
    }
}