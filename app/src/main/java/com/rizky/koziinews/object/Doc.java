package com.rizky.koziinews.object;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rizky.koziinews.data.database.DB;

import java.util.List;

@Entity(tableName = DB.TABLE_NEWS)
public class Doc {
    @Ignore
    public String abstraction;

    @ColumnInfo(name = DB.COLUMN_WEB_URI)
    public String web_url;

    @ColumnInfo(name = DB.COLUMN_SNIPPET)
    public String snippet;

    @Ignore
    public String lead_paragraph;

    @Ignore
    public String source;

    @Ignore
    public List<Multimedia> multimedia;

    @Ignore
    public Headline headline;

    @Ignore
    public List<Keyword> keywords;

    @ColumnInfo(name = DB.COLUMN_DATE_CREATED)
    public String pub_date;

    @Ignore
    public String document_type;

    @Ignore
    public String news_desk;

    @ColumnInfo(name = DB.COLUMN_SECTION)
    public String section_name;

    @Ignore
    public Byline byline;

    @Ignore
    public String type_of_material;

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = DB.COLUMN_ID)
    public String _id;

    @Ignore
    public int word_count;

    @Ignore
    public String uri;

    @Ignore
    public String subsection_name;

    @ColumnInfo(name = DB.COLUMN_HEADLINE)
    public String headline_main;

    @ColumnInfo(name = DB.COLUMN_IMG_BITMAP)
    private String img_bitmap;

    public Doc() {

    }

    public String getAbstraction() {
        return abstraction;
    }

    public void setAbstraction(String abstraction) {
        this.abstraction = abstraction;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getLead_paragraph() {
        return lead_paragraph;
    }

    public void setLead_paragraph(String lead_paragraph) {
        this.lead_paragraph = lead_paragraph;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Multimedia> getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(List<Multimedia> multimedia) {
        this.multimedia = multimedia;
    }

    public Headline getHeadline() {
        return headline;
    }

    public void setHeadline(Headline headline) {
        this.headline = headline;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }

    public String getPub_date() {
        return pub_date;
    }

    public void setPub_date(String pub_date) {
        this.pub_date = pub_date;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getNews_desk() {
        return news_desk;
    }

    public void setNews_desk(String news_desk) {
        this.news_desk = news_desk;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public Byline getByline() {
        return byline;
    }

    public void setByline(Byline byline) {
        this.byline = byline;
    }

    public String getType_of_material() {
        return type_of_material;
    }

    public void setType_of_material(String type_of_material) {
        this.type_of_material = type_of_material;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getWord_count() {
        return word_count;
    }

    public void setWord_count(int word_count) {
        this.word_count = word_count;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getSubsection_name() {
        return subsection_name;
    }

    public void setSubsection_name(String subsection_name) {
        this.subsection_name = subsection_name;
    }

    public String getHeadline_main() {
        return headline_main;
    }

    public void setHeadline_main(String headline_main) {
        this.headline_main = headline_main;
    }

    public String getImg_bitmap() {
        return img_bitmap;
    }

    public void setImg_bitmap(String img_bitmap) {
        this.img_bitmap = img_bitmap;
    }
}
