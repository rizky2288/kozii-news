package com.rizky.koziinews.object;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.rizky.koziinews.KoziiMethods;
import com.rizky.koziinews.data.database.DB;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = DB.TABLE_SEARCH)
public class Search {
    @ColumnInfo(name = DB.COLUMN_DATE_CREATED)
    private String datetime;
    @PrimaryKey()
    @ColumnInfo(name = DB.COLUMN_SEARCH_HISTORY)
    @NonNull
    private String search_history;

    public Search(@NotNull String search_history) {
        this.search_history = search_history;
        this.datetime = KoziiMethods.GetDateNowString();
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    @NonNull
    public String getSearch_history() {
        return search_history;
    }

    public void setSearch_history(@NonNull String search_history) {
        this.search_history = search_history;
    }
}
